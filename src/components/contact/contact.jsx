"use client";
import React from "react";
import { FiInstagram, FiLinkedin, FiGitlab } from "react-icons/fi";
import { TbBrandWhatsapp } from "react-icons/tb";

export default function Contact() {
  const sosmeds = [
    {
      name: "WhatsApp",
      icon: <TbBrandWhatsapp size={20} />,
      link: "https://wa.me/6282137260187",
    },
    {
      name: "Gitlab",
      icon: <FiGitlab size={20} />,
      link: "https://gitlab.com/aldinugroholutfi",
    },
    {
      name: "Instagram",
      icon: <FiInstagram size={20} />,
      link: "https://www.instagram.com/lutfialdinugroho/",
    },
    {
      name: "Linkedin",
      icon: <FiLinkedin size={20} />,
      link: "https://www.linkedin.com/in/lutfi-aldi-nugroho-266018145/",
    },
  ];
  return (
    <div className="hidden lg:block">
      <div className="fixed bottom-0 left-0 w-[105px]">
        <div className="flex flex-col items-center" data-aos="fade-up">
          {sosmeds.map((sosmed, index) => (
            <div
              key={index}
              data-aos="fade-right"
              data-aos-delay={`${index}00`}
              data-aos-duration="600"
            >
              <div
                className="cursor-pointer px-2 py-3 text-primary transition-all duration-300 hover:-translate-y-1 hover:text-accent"
                title={sosmed.name}
              >
                <a
                  href={sosmed.link}
                  target="_blank"
                  rel="noreferrer"
                  aria-label={sosmed.name}
                >
                  {sosmed.icon}
                </a>
              </div>
            </div>
          ))}
          <div className="mt-4 h-20 w-[1px] bg-primary"></div>
        </div>
      </div>
    </div>
  );
}
