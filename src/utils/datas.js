// Stack Icons
import htmlIcon from "@icons/html.svg";
import cssIcon from "@icons/css.svg";
import jsIcon from "@icons/js.svg";
import phpIcon from "@icons/php.svg";
import sassIcon from "@icons/sass.svg";
import bootstrapIcon from "@icons/bootstrap.svg";
import tailwindIcon from "@icons/tailwind.svg";
import reactIcon from "@icons/react.svg";
import nextjsIcon from "@icons/nextjs.svg";
import threejsIcon from "@icons/threejs.svg";
import laravelIcon from "@icons/laravel.svg";
import nodejsIcon from "@icons/nodejs.svg";
import npmIcon from "@icons/npm.svg";
import yarnIcon from "@icons/yarn.svg";
import blenderIcon from "@icons/blender.svg";
import figmaIcon from "@icons/figma.svg";
import gitIcon from "@icons/git.svg";
import githubIcon from "@icons/github.svg";
import mysqlIcon from "@icons/mysql.svg";
import splineIcon from "@icons/spline.svg";
import vscodeIcon from "@icons/vscode.svg";
import vite from "@icons/vite.svg";
import webpackIcon from "@icons/webpack.svg";
import expressIcon from "@icons/express.svg";
import mongodbIcon from "@icons/mongodb.svg";
import typescriptIcon from "@icons/typescript.svg";
import expoIcon from "@icons/expo.svg";
import reduxIcon from "@icons/redux.svg";
import storageIcon from "@icons/storage.svg";
import firestoreIcon from "@icons/firestore.svg";
import fcmIcon from "@icons/fcm.svg";
import pusherIcon from "@icons/pusher.svg";
import socketioIcon from "@icons/socketio.svg";
import prismaIcon from "@icons/prisma.svg";
import gitlab from "@icons/gitlab.svg";
import webstorm from "@icons/webstorm.svg";
import intellij from "@icons/intellij.svg";
import datagrip from "@icons/datagrip.svg";
import reactQueryIcon from "@icons/react-query.svg";
import formikIcon from "@icons/formik.png";

// Project Images
import mulih from "@images/mulih.webp";
import travel from "@images/travel.webp";
import bookshelf from "@images/bookshelf.webp";
import notes from "@images/notes.webp";
import treede from "@images/3d.webp";
import abinawa from "@images/abinawa.webp";
import inpoums from "@images/inpoums.webp";
import visitlaunch from "@images/visitlaunch.webp";
import dpm from "@images/dpm.webp";
import kilapin from "@images/kilapin.webp";
import visitweb from "@images/visitweb.webp";
import adminkilapin from "@images/admin-kilapin.webp";
import admindpm from "@images/admin-dpm.webp";
import adminsulut from "@images/admin-sulut.webp";
import fwen from "@images/fwen.webp";
import abdul from "@images/abdul.webp";
import abdulv2 from "@images/abdulv2.webp";
import artix from "@images/artix.webp";
import portfolio_v1 from "@images/portfolio-v1.webp";
import bertumbuh from "@images/bertumbuh.webp";
import coffe_bean from "@images/coffe-bean.webp";
import dapbunda from "@images/dapbunda.webp";
import hpcjapan from "@images/hpc-japan.webp";
import ceritain from "@images/ceritain.webp";
import pinarak_resto from "@images/pinarak_resto.webp";

// Gif Images
import dapbundaGif from "@gif/dapbunda.gif";
import coffebeanGif from "@gif/coffe-bean.gif";
import portfolioGif from "@gif/portfolio.gif";
import notesGif from "@gif/notes.gif";
import abinawaGif from "@gif/abinawa.gif";
import bookshelfGif from "@gif/bookshelf.gif";
import sakurajapanGif from "@gif/sakura-japan.gif";
import ceritainGif from "@gif/ceritain.gif";
import pinarakrestoGif from "@gif/pinarak-resto.gif";

const tools = {
  Git: { name: "Git", src: gitIcon, level: "Version control" },
  Webstorm: { name: "Webstorm", src: webstorm, level: "IDE" },
  IntellijIdea: { name: "IntellijIdea", src: intellij, level: "IDE Mobile" },
  Datagrip: { name: "Datagrip", src: datagrip, level: "IDE Database" },
  Figma: { name: "Figma", src: figmaIcon, level: "UIUX Design tool" }
};

const stacks = {
  JS: { name: "Javascript", src: jsIcon, level: "Expert" },
  Typescript: { name: "Typescript", src: typescriptIcon, level: "Advanced" },
  Express: { name: "ExpressJS", src: expressIcon, level: "Expert" },
  MySQL: { name: "MySQL", src: mysqlIcon, level: "Expert" },
  Prisma: { name: "Prisma", src: prismaIcon, level: "Expert" },
  MongoDB: { name: "MongoDB", src: mongodbIcon, level: "Expert" },
  React: { name: "ReactJS", src: reactIcon, level: "Expert" },
  ReactNativeExpo: { name: "Expo", src: expoIcon, level: "Expert" },
  ReactNativeCli: { name: "React Native", src: reactIcon, level: "Expert" },
  NextJS: { name: "NextJS", src: nextjsIcon, level: "Expert" },
  Tailwind: { name: "TailwindCSS", src: tailwindIcon, level: "Expert" },
  Redux: { name: "Redux", src: reduxIcon, level: "Expert" },
  AsyncStorage: { name: "AsyncStorage", src: storageIcon, level: "Expert" },
  FireStore: { name: "FireStore", src: firestoreIcon, level: "Expert" },
  Fcm: { name: "FCM", src: fcmIcon, level: "Expert" },
  Pusher: { name: "Pusher", src: pusherIcon, level: "Expert" },
  Socketio: { name: "Socket.io", src: socketioIcon, level: "Expert" }
};

const projects = [
  {
    id: 1,
    name: "Admin Page Visit North",
    desc: "The Admin page for the Visit North Sulawesi project plays a crucial role in managing various aspects of the application, particularly when it comes to accommodations and the application's dictionary. It serves as a centralized hub for inputting and updating information related to accommodations, ensuring that our users have access to the latest and most accurate data.",
    image: adminsulut,
    web: "http://admin.visit-northsulawesi.com/",
    repo: "",
    stack: "MongoDB, ExpressJs, React, NodeJs, Prisma",
    gif: "",
  },
  {
    id: 2,
    name: "Dashboard ASP Ericsson.",
    desc: "The admin dashboard serves as a comprehensive monitoring tool for Key Performance Indicators (KPIs) related to our ASP Engineers. It enables efficient tracking and analysis of individual engineer performance, allowing us to closely monitor their progress and contributions to our projects. This valuable feature helps us ensure that each engineer is on track and aligned with our goals, ultimately leading to improved efficiency and results in our engineering department.",
    image: admindpm,
    web: "https://digitalprojectmanagement.ericsson.net/",
    repo: "",
    stack: "React, Tailwind, MongoDB, ExpressJs, React, NodeJs, Prisma",
    gif: "",
  },
  {
    id: 3,
    name: "Admin Page Kilapin",
    desc: "The admin page for Kilapin is designed to provide a robust backend management system for handling various events and ensuring their smooth operation. With this powerful tool, you can effortlessly maintain and manage all aspects of your events, from scheduling and participant registration to content updates and analytics tracking. Kilapin's admin page empowers you to have complete control over your events, allowing you to make real-time adjustments and optimizations as needed. This user-friendly interface simplifies the process of event management, enhancing efficiency and ensuring a seamless experience for both organizers and participants. Whether it's monitoring attendance, updating event details, or analyzing performance metrics, Kilapin's admin page is your go-to solution for effective event maintenance.",
    image: adminkilapin,
    web: "http://admin.kilapin.com/",
    repo: "",
    stack: "MongoDB, ExpressJs, React, NodeJs, Prisma",
    gif: "",
  },
  {
    id: 4,
    name: "Visit North Sulawesi Web",
    desc: "Visit North Sulawesi is the ultimate tourism application that takes your travel experience to the next level in this captivating Indonesian haven. We understand the challenges of planning a perfect trip, and that's why our app combines cutting-edge AI machine learning with a vast database of attractions and services to make your journey seamless and memorable.",
    image: visitweb,
    web: "http://visit-northsulawesi.com/",
    repo: "",
    stack: "React, Tailwind, MongoDB, ExpressJs, React, NodeJs, Prisma",
    gif: "",
  },
  {
    id: 5,
    name: "Kilapin",
    desc: "KILAPIN, a one-stop solution for technology-based sanitation services.",
    image: kilapin,
    web: "https://kilapin.com/",
    repo: "",
    stack: "React, React-router, TailwindCSS",
    gif: "",
  },
  {
    id: 6,
    name: "Ericsson DPM",
    desc: "DPM (Digital Project Management) is a tool developed by Ericsson MOAI Team to help manage daily acitivites of Project, Warehouse, ASP, and DSP Team",
    image: dpm,
    web: "",
    repo: "",
    stack: "React-Native, MySQL, ExpressJs, React, NodeJs",
    gif: "",
  },
  {
    id: 7,
    name: "Visit North Sulawesi App",
    desc: "Visit North Sulawesi is the ultimate tourism application that takes your travel experience to the next level in this captivating Indonesian haven. We understand the challenges of planning a perfect trip, and that's why our app combines cutting-edge AI machine learning with a vast database of attractions and services to make your journey seamless and memorable.",
    image: visitlaunch,
    web: "https://play.google.com/store/apps/details?id=com.visit.northsulawesi&hl=en-ID",
    repo: "",
    stack: "React-Native, MongoDB, ExpressJs, React, NodeJs, Prisma",
    gif: "",
  },
  {
    id: 8,
    name: "Absendulu (BioMetrics)",
    desc: "HRMS Software that integrates with face recognition devices (ZkTeco Biometric Attendance Device) for attendance and payroll management. Witnessing the adoption of this application by reputable companies such as Tempo Scan and HokaHokaBento has been immensely gratifying, showcasing the impact of my work in real-world scenarios.",
    image: abdul,
    web: "https://absendulu.co.id/server-config",
    repo: "",
    stack: "Postgresql, ExpressJs, React, Kubernetes, RabbitMQ, Redis, Portainer",
    gif: "",
  },
  {
    id: 9,
    name: "Absendulu (Mobile Apps)",
    desc: "HRMS Software that integrates with Mobile Apps for attendance and payroll management. Witnessing the adoption of this application by reputable companies such as Tempo Scan and HokaHokaBento has been immensely gratifying, showcasing the impact of my work in real-world scenarios.",
    image: abdulv2,
    web: "https://webapp.absendulu.co.id/Auth/Login?ReturnUrl=%2F",
    repo: "",
    stack: ".Net Core, ASP, MySQL, Flutter, Azure",
    gif: "",
  },
  {
    id: 10,
    name: "Artix-ERP Enterprise",
    desc: "ERP software that allows developers to build custom business applications with features like modular architecture, role-based permissions, and a built-in REST API. Its key strengths are flexibility, ease of customization, and robust community support, making it ideal for creating scalable and tailored solutions for various business needs.",
    image: artix,
    web: "https://internal.artix-labs.com/app/home",
    repo: "",
    stack: "Python, VueJs, Redis, RabbitMQ, mariaDB",
    gif: "",
  },
  {
    id: 11,
    name: "Fwen.io",
    desc: "FWEN is a platform that uses blockchain to help creators and fans connect, collaborate, and earn rewards. It's all about making sure everyone gets a fair share for what they bring to the table. Are you a content creator or a fan looking to earn from your creativity or engagement? FWEN is here to help make that happen. It's straightforward, fun, and designed with YOU in mind.",
    image: fwen,
    web: "https://app.fwen.io/",
    repo: "",
    stack: "MongoDB, ExpressJs, NextJs, Web3, Solana",
    gif: "",
  },
];

export { tools, stacks, projects };
