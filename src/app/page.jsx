import Dominos from "./models";
import Image from "next/image";
import secret from "@images/secret.webp";

export const metadata = {
  title: "Lutfi Aldi Nugroho",
  description:
    "Lutfi Aldi Nugroho is a Full-Stack Software Engineer.",
};

export default function Home() {
  return (
    <div>
      <div className="flex h-[80vh] w-full flex-col justify-center px-0 pt-44 leading-relaxed md:h-screen md:pt-16 lg:px-[70px] lg:pt-12 ">
        <p
          className="ml-1 hidden pb-3 text-sm text-accent md:block md:pb-7 md:text-base"
          data-aos="fade-up"
        >
          Hi, my name is
        </p>
        <h1
          className="pb-1 text-4xl font-semibold text-secondary md:mt-0 md:pb-4 md:text-5xl lg:text-6xl"
          data-aos="fade-up"
        >
          Lutfi Aldi Nugroho.
        </h1>
        <h1
          className="pb-4 text-4xl font-semibold text-primary md:pb-8 md:text-5xl lg:text-6xl"
          data-aos="fade-up"
        >
          Full-Stack Software Engineer.
        </h1>
        <p
          className="w-full text-sm leading-relaxed text-primary md:w-[540px] md:text-base"
          data-aos="zoom-in-up"
        >
          A seasoned software engineer with a passion for pushing the boundaries of innovation in the Telecommunication and IT domains.
          My journey is defined by a deep-rooted love for programming and an unwavering commitment to mastering my craft,
          enabling me to navigate and conquer complex technical challenges with ease.
        </p>
        {/*<div*/}
        {/*  className="mt-10 text-sm md:mt-14"*/}
        {/*  data-aos="flip-up"*/}
        {/*  data-aos-duration="600"*/}
        {/*>*/}
        {/*  <a*/}
        {/*    className="rounded-[4px] border-2 border-accent px-7 py-5 text-accent transition-all duration-300 hover:bg-accent hover:bg-opacity-10"*/}
        {/*    href="https://drive.google.com/drive/folders/1QoujbRghInMFjV5ECmudG3Mho-aOdngI?usp=sharing"*/}
        {/*    target="_blank"*/}
        {/*    rel="noreferrer"*/}
        {/*  >*/}
        {/*    <button>View my cv here!</button>*/}
        {/*  </a>*/}
        {/*</div>*/}
      </div>
      <div className="flex h-[650px] w-full flex-col items-center justify-center py-24 text-center ">
        <div className="absolute mb-[-430px] mr-[10px] md:mb-[400px] md:mr-[200px] lg:mr-[300px] xl:mr-[480px]">
          <Image
            src={secret}
            alt="secret game"
            className="h-[150px] w-auto opacity-20 md:h-[200px] lg:h-[250px]"
          />
        </div>
        <div className="z-40 mb-0 md:mb-10">
          <h2
            className="pb-2 text-3xl font-bold leading-tight text-secondary md:pb-0 md:text-4xl md:leading-relaxed lg:text-5xl lg:leading-relaxed"
            data-aos="zoom-in-up"
            data-aos-duration="300"
          >
            Crafting modern,
            <br />
            Cohesive & Intuitive Web and Mobile Solutions.
          </h2>
          <p
            className="text-sm text-primary md:text-base"
            data-aos="fade-up"
            data-aos-duration="300"
          >
            In the realm of Information Technologies, I thrive on solving complex puzzles, delivering elegant solutions, and staying one step ahead in this ever-evolving digital landscape.
          </p>
        </div>
        <div className="bg-base-100 absolute z-0 m-auto h-[600px] w-[350px] opacity-25 md:w-[672px] lg:w-[825px] xl:w-[1080px]">
          <Dominos />
        </div>
      </div>
    </div>
  );
}
