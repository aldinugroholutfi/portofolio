import bootstrapIcon from "@icons/bootstrap.svg";
import tailwindIcon from "@icons/tailwind.svg";
import reactIcon from "@icons/react.svg";
import nextjsIcon from "@icons/nextjs.svg";
import laravelIcon from "@icons/laravel.svg";
import nodejsIcon from "@icons/nodejs.svg";
import expressIcon from "@icons/express.svg";
import mongodbIcon from "@icons/mongodb.svg";
import sql from "@icons/mysql.svg";
import prisma from "@icons/prisma.svg";
import reactnative from "@icons/react.svg";
import expo from "@icons/expo.svg";
import Image from "next/image";

export const ContentWorkExperience = () => {
  const datas = [
    {
      title: "FullStack Software Engineer",
      url: "https://www.linkedin.com/company/prodigium-id/",
      company: "PT. PRODIGIUM",
      date: "May 2023 - Present",
      description: (
        <p>
            At PT. Prodigium, I've embraced the role of a Technical Lead Full Stack Developer,
            where I'm spearheading the development of the Visit North Sulawesi App/Web.
            Additionally, I'm leading the Kilapin App/Web project.
            <ul>
                <li>
                    <a
                        href="https://play.google.com/store/apps/details?id=com.visit.northsulawesi&hl=en-ID"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Visit North Sulawesi on Google Play.
                    </a>
                </li>
                <li>
                    <a
                        href="http://visit-northsulawesi.com/"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Visit North Sulawesi Website.
                    </a>
                </li>
                <li>
                    <a
                        href="http://admin.visit-northsulawesi.com/"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Visit North Sulawesi Admin Page.
                    </a>
                </li>
                <li>
                    <a
                        href="https://kilapin.com/"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Kilapin Website.
                    </a>
                </li>
                <li>
                    <a
                        href="http://admin.kilapin.com/"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Kilapin Admin Page.
                    </a>
                </li>
            </ul>
        </p>

      ),
      techs: [
        {
          name: "MongoDB",
          icon: mongodbIcon,
        },{
          name: "Prisma",
          icon: prisma,
        },
        {
          name: "Express",
          icon: expressIcon,
        },
        {
          name: "React",
          icon: reactIcon,
        },
        {
          name: "NodeJS",
          icon: nodejsIcon,
        },
          {
          name: "React Native",
          icon: reactnative,
        },{
          name: "Expo",
          icon: expo,
        },
      ],
    },
    {
      title: "FullStack Software Engineer",
      url: "https://www.linkedin.com/company/ericsson/",
      company: "ERICSSON",
      date: "Aug 2021 - May 2023",
      description: (
        <p>
            In 2021, I returned to Ericsson Indonesia, my previous workplace, with a renewed focus on further improving and enhancing the features of the Digital Project Management (DPM) application,
            a project that I had previously played a key role in developing.

            <ul>
                <li>
                    <a
                        href="https://play.google.com/store/apps/details?id=com.ericsson.dpm_android&hl=en-ID"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Ericsson DPM App on Google Play.
                    </a>
                </li>
                <li>
                    <a
                        href="https://digitalprojectmanagement.ericsson.net/"
                        className="text-underline"
                        target="_blank"
                        rel="noreferrer"
                    >
                        Ericsson DPM App Web.
                    </a>
                </li>
            </ul>
        </p>
      ),
      techs: [
          {
              name: "NodeJS",
              icon: nodejsIcon,
          },{
              name: "MySQL",
              icon: sql,
          },
          {
              name: "React Native",
              icon: reactnative,
          },{
              name: "Expo",
              icon: expo,
          },
        {
          name: "NextJS",
          icon: nextjsIcon,
        },
        {
          name: "TailwindCSS",
          icon: tailwindIcon,
        },
      ],
    },
    {
      title: "Fullstack Web Developer",
      url: "https://www.linkedin.com/company/wijaya-karya-beton-pt/",
      company: "PT. WIJAYA KARYA BETON",
      date: "May 2019 - Aug 2021",
      description: (
        <p>
            During my tenure at PT. Wijaya Karya, my focus was on enhancing and improving existing systems, particularly in finance.
            While I didn't create new products from scratch, my contributions centered on performance optimization, feature enhancements, and query optimization.
            It was a period of honing my skills and working with a variety of technologies.
            Notably, I added a new module to the finance system, introducing features like journaling and neraca.
        </p>
      ),
      techs: [
        {
          name: "Laravel",
          icon: laravelIcon,
        },
          {
              name: "MySQL",
              icon: sql,
          },
      ],
    },
      {
          title: "FullStack Software Engineer",
          url: "https://www.linkedin.com/company/ericsson/",
          company: "ERICSSON",
          date: "March 2017 - May 2019",
          description: (
              <p>
                  As a Software Engineer at Ericsson Indonesia, I embarked on a transformative journey in the realm of telecommunications by working on the Digital Project Management (DPM) System. This system served as the connective tissue between field engineers, remote engineers, and service providers, facilitating seamless communication and collaboration.
                  <br/>
                  <ul>
                      <li>
                          <a
                              href="https://play.google.com/store/apps/details?id=com.ericsson.dpm_android&hl=en-ID"
                              className="text-underline"
                              target="_blank"
                              rel="noreferrer"
                          >
                              Ericsson DPM App on Google Play.
                          </a>
                      </li>
                      <li>
                          <a
                              href="https://digitalprojectmanagement.ericsson.net/"
                              className="text-underline"
                              target="_blank"
                              rel="noreferrer"
                          >
                              Ericsson DPM App Web.
                          </a>
                      </li>
                  </ul>
              </p>
          ),
          techs: [
              {
                  name: "NodeJS",
                  icon: nodejsIcon,
              },{
                  name: "MySQL",
                  icon: sql,
              },
              {
                  name: "React Native",
                  icon: reactnative,
              },{
                  name: "Expo",
                  icon: expo,
              },
              {
                  name: "NextJS",
                  icon: nextjsIcon,
              },
              {
                  name: "TailwindCSS",
                  icon: tailwindIcon,
              },
          ],
      },
  ];

  return (
    <div className="flex flex-col gap-8 text-primary">
      {datas.map((data, index) => (
        <div key={index}>
          <h3 className="text-lg font-medium leading-loose text-secondary md:text-xl">
            {data.title}
            <span className="text-base text-accent">
              {" "}
              @
              <a
                href={data.url}
                target="_blank"
                rel="noreferrer"
                className="text-underline"
              >
                {data.company}
              </a>
            </span>
          </h3>
          <p className="pb-5 font-mono text-sm lg:text-base">{data.date}</p>
          <div className="flex list-inside list-disc flex-col gap-4 text-sm leading-relaxed lg:text-base">
            {data.description}
          </div>
          <div className="mt-2">
            <h4 className="mb-1 font-medium">Technologies used</h4>
            <div className="flex flex-wrap gap-2">
              {data.techs.map((tech, index) => (
                <div
                  key={index}
                  className="rounded-md bg-accent/10 p-1"
                  title={tech.name}
                >
                  <Image
                    src={tech.icon}
                    alt={tech.icon}
                    width={28}
                    height={28}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
