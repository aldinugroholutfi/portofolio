export const ContentEducation = () => {
  return (
    <div className="text-primary">
      <h3 className="text-lg font-medium leading-loose text-secondary md:text-xl">
        Informatics Engineering
        <span className="text-base text-accent">
          {" "}
          @
          <a
            href="https://telkomuniversity.ac.id/s1-informatika/"
            target="_blank"
            rel="noreferrer"
            className="text-underline"
          >
            Telkom University
          </a>
        </span>
      </h3>
      <p className="pb-5 font-mono text-sm lg:text-base">Aug 2013 - March 2017</p>
      <ul className="flex list-inside list-disc flex-col gap-4 text-sm leading-relaxed lg:text-base">
        <li>
          I graduated from Telkom University in 2017 with a GPA of 3.20. My specialization was in the field of computing and artificial intelligence.
        </li>
      </ul>
    </div>
  );
};
