import Image from "next/image";
import aldi from "@images/aldi.webp";
import { tools, stacks } from "@/utils/datas";
import CardAbout from "@/components/card/card-about";
import Experience from "./experience/experience";

export const metadata = {
  title: "About | Lutfi Aldi N.",
  description:
    "About Lutfi Aldi N. | I'm a seasoned full-stack software engineer with over 6 years of experience in crafting both mobile applications and web solutions.",
};

export default function About() {
  return (
    <section>
      <div className="mt-32 flex h-auto flex-col items-center justify-between lg:mt-6 lg:h-screen lg:flex-row xl:mt-0">
        <div className="flex flex-col justify-center text-primary">
          <div className="flex items-center gap-5 pb-10">
            <h1
              className="text-xl font-semibold text-secondary lg:text-2xl"
              data-aos="fade-left"
              data-aos-duration="300"
            >
              <span className="font-mono text-lg font-normal text-accent lg:text-xl">
                02.{" "}
              </span>
              About Me
            </h1>
            <div
              className="h-[1px] w-32 bg-primary md:w-96"
              data-aos="zoom-in-left"
              data-aos-duration="600"
            ></div>
          </div>
          <div className="flex flex-col gap-5 pr-0 text-sm md:text-base lg:pr-10 xl:pr-0">
            <p data-aos="zoom-in-right">
              I'm a seasoned full-stack software engineer with over 6 years of experience in crafting both mobile applications and web solutions.
            </p>
            <p data-aos="zoom-in-right" data-aos-delay="100">
              My passion lies in creating tools and applications that enhance the lives of users,
              making their experiences simpler and more efficient.
              I thrive on solving complex problems from unique perspectives, always striving to simplify processes.
            </p>
            <p data-aos="zoom-in-right" data-aos-delay="200">
              I have a keen interest in cutting-edge technologies like AI and Web3 development,
              believing there's ample opportunity for me to contribute and innovate in these evolving fields.
            </p>
            {/*<p data-aos="zoom-in-right" data-aos-delay="300">*/}
            {/*  And oh, there's a little twist &#x2013; I'm also into the world of*/}
            {/*  3D modeling, though I confess I might not be diving into its*/}
            {/*  depths just yet. It's like my side quest that adds a dash of*/}
            {/*  excitement.*/}
            {/*</p>*/}
          </div>
        </div>
        <div
          className="-my-20 pl-0 md:-my-40 lg:-my-0 xl:pl-24"
          data-aos="zoom-in"
          data-aos-duration="300"
        >
          <div className="grayscale transition-all duration-500 hover:grayscale-0">
            <Image
              src={aldi}
              alt="aldi"
              height={1000}
              className="scale-50 lg:scale-100 2xl:scale-75"
            />
          </div>
        </div>
      </div>
      <div className="pt-[100px] pb-[50px] lg:pb-[60px]">
        <Experience />
      </div>
      <div>
        <div className="flex items-center gap-2 pb-12">
          <div
            className="h-[1px] w-12 bg-primary"
            data-aos="zoom-in-right"
            data-aos-duration="300"
            data-aos-once="true"
          ></div>
          <h1
            className="text-xl font-semibold text-secondary md:text-2xl"
            data-aos="fade-right"
            data-aos-duration="600"
            data-aos-once="true"
          >
            Tech stack
          </h1>
        </div>
        <div className="grid grid-cols-2 gap-4 pb-32 md:grid-cols-3 md:gap-8 xl:grid-cols-4 xl:gap-10 2xl:gap-12">
          {Object.keys(stacks).map((stack, index) => (
            <div
              key={index}
              data-aos="zoom-in"
              data-aos-delay={`${index}00`}
              data-aos-duration="300"
              data-aos-once="true"
            >
              <CardAbout
                src={stacks[stack].src}
                name={stacks[stack].name}
                level={stacks[stack].level}
              />
            </div>
          ))}
        </div>
        <div className="flex items-center gap-2 pb-12">
          <div
            className="h-[1px] w-12 bg-primary"
            data-aos="zoom-in-right"
            data-aos-duration="300"
            data-aos-once="true"
          ></div>
          <h1
            className="text-xl font-semibold text-secondary md:text-2xl"
            data-aos="fade-right"
            data-aos-duration="600"
            data-aos-once="true"
          >
            Tools used
          </h1>
        </div>
        <div className="grid grid-cols-2 gap-4 pb-32 md:grid-cols-3 md:gap-8 xl:grid-cols-4 xl:gap-10 2xl:gap-12">
          {Object.keys(tools).map((tool, index) => (
            <div
              key={index}
              data-aos="zoom-in"
              data-aos-delay={`${index}00`}
              data-aos-duration="300"
              data-aos-once="true"
            >
              <CardAbout
                src={tools[tool].src}
                name={tools[tool].name}
                level={tools[tool].level}
                key={index}
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
